package utils;


import java.io.IOException;
import java.lang.reflect.Method;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

public class ProjectWrappers  extends GenericWrappers{
	
	
		public String testName, description;
		public String browser,env;
		public String author;
		public String category;
		public Method method;
		
		@BeforeSuite
		public void beforeSuite() throws IOException {
			startReport();
		}
		
	
		@BeforeTest
		public void beforeTest() {
		//this.env=environment;	
		}
		
		
		@BeforeMethod
		public void beforeMethod() {
			startTest(testName, description);
			//invokeApp(env,true);
		}
		
		
		@AfterMethod
		public void afterMethod() {
		System.out.println("Test Complete");
			//closeAllBrowsers();	
		}
		
		
		@AfterClass
		public void afterClass() {
			endTest();
			closeAllBrowsers();
		}
		
		@AfterTest
		public void afterTest() {
		//not to use	
		}
		
		
		@AfterSuite
		public void afterSuite() {
		endReport();	
		}
		

	}
