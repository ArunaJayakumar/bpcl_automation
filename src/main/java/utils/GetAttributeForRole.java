package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.apache.commons.lang3.text.WordUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;

public class GetAttributeForRole {


	public static ArrayList<String> getAttributes(String sheetName, String role) throws IOException {
		String attributeList = null;
		try {
			FileInputStream fis = new FileInputStream("./Data/AttributesRoles.xlsx");

			XSSFWorkbook wb = new XSSFWorkbook(fis);
			XSSFSheet sheet=	wb.getSheet(sheetName);
			int rowCount=sheet.getLastRowNum();
			int rowNumber=0;
			for(int i=0;i<rowCount+1;i++)
			{
				XSSFRow row=sheet.getRow(i);
				String roleAtI=sheet.getRow(i).getCell(0).getStringCellValue();
				if(roleAtI.equals(role)) {
					rowNumber=i;
					break;
				}
			}

			attributeList=sheet.getRow(rowNumber).getCell(1).getStringCellValue();
			wb.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		String finalAttributeList=attributeList.replaceAll("_", " ");
		System.out.println("***Returning TestData***");
		String[] attributeArray=finalAttributeList.split(",");
		int len=attributeArray.length;
		String[] capArray=new String[len];
		for (int i=0;i<len;i++)
		{
			capArray[i]=	WordUtils.capitalize(attributeArray[i]);
		}
		ArrayList<String> excelAttList=new ArrayList<String>(Arrays.asList(capArray));
		Collections.sort(excelAttList);

		return excelAttList;
	}


	public static ArrayList<String> getAttributesWOBlacklist(String sheetName, String role) throws IOException {
		String attributeList = null;
		String blackList=null;
		try {
			FileInputStream fis = new FileInputStream("./Data/AttributesRoles.xlsx");

			XSSFWorkbook wb = new XSSFWorkbook(fis);
			XSSFSheet sheet=	wb.getSheet(sheetName);
			int rowCount=sheet.getPhysicalNumberOfRows();
			//	getLastRowNum();
			System.out.println("Total number of rows: "+rowCount);
			int rowNumber=0;
			for(int i=0;i<rowCount;i++)
			{
				XSSFRow row=sheet.getRow(i);
				String roleAtI=sheet.getRow(i).getCell(0).getStringCellValue();
				if(roleAtI.equals(role)) {
					rowNumber=i;
					break;
				}
			}
			System.out.println("RowNumber is: "+rowNumber);	
			attributeList=sheet.getRow(rowNumber).getCell(1).getStringCellValue();
			blackList=sheet.getRow(rowCount-1).getCell(1).getStringCellValue();
			wb.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}

		//Customize Attribute list
		String finalAttributeList=attributeList.replaceAll("_", " ");
		String[] attributeArray=finalAttributeList.split(",");
		System.out.println("String array is: \n"+attributeArray);
		int len=attributeArray.length;
		String[] capArray=new String[len];
		for (int i=0;i<len;i++)
		{
			//=attributeArray[i].
			capArray[i]=	WordUtils.capitalize(attributeArray[i]);
			//if(capArray[i].contains("Nsat "))
		}
		System.out.println("Captial array is: \n"+Arrays.toString(capArray));
		ArrayList<String> excelAttList=new ArrayList<String>(Arrays.asList(capArray));

		//Customize blacklisted Attributes
		String finalBlackList=blackList.replaceAll("_", " ");
		String[] blackArray=finalBlackList.split(",");
		int lenBlack=blackArray.length;
		System.out.println("Blacklist count is: "+lenBlack);
		String[] capArrayBlack=new String[lenBlack];
		for (int i=0;i<lenBlack;i++)
		{
			capArrayBlack[i]=	WordUtils.capitalize(blackArray[i]);
		}
		System.out.println("Blacklist Captial array is: \n"+Arrays.toString(capArrayBlack));
		ArrayList<String> excelBlackList=new ArrayList<String>(Arrays.asList(capArrayBlack));

		excelAttList.removeAll(excelBlackList);
		Collections.sort(excelAttList);
		return excelAttList;
	}


}
