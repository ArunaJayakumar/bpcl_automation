package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.jboss.aerogear.security.otp.Totp;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.ElementNotSelectableException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;


public class GenericWrappers extends Reports{
	public WebDriver driver;
	public String testName, description;
	public String browser;
	public String author;
	public String category;
	public Method method;
	public int i=1;
	
	//DesiredCapabilities dc = DesiredCapabilities.chrome();
	
	//dc.setBrowserName("chrome");
	
	public WebDriver getDriver() throws MalformedURLException {
		//WebDriverManager.chromedriver().setup();
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver");
		DesiredCapabilities dc = DesiredCapabilities.chrome();
		dc.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		ChromeOptions options = new ChromeOptions();
	options.addArguments("--no-sandbox","--headless","incognito","--window-size=1920,1080");
			//"--auto-open-devtools-for-tabs");
	options.merge(dc);
	//driver=new RemoteWebDriver(new URL("http://localhost:9515"), DesiredCapabilities.chrome());
	driver=new ChromeDriver(options);        
	System.out.println("Driver created");
	return this.driver;
	}

	
		
	public void invokeApp(String testName,boolean flag) throws InterruptedException, IOException {
		System.out.println("Invoking application");		 
		Properties prop=new Properties();
      	BufferedReader reader=new BufferedReader(new FileReader("./Data/login.properties"));
    	prop.load(reader);  
    	String env=prop.getProperty("env");
    	String user=prop.getProperty(env+"username");
    	String pwd=prop.getProperty(env+"password");  
    
	
		String url=null;
		if(env.equalsIgnoreCase("uat"))
			url="https://strike.bpclcloud9.com/";
		else if(env.equalsIgnoreCase("globedev"))
			url="https://uup-dev.globe.com.ph/cpp/audiences/audienceDiscovery";
		else if(env.equalsIgnoreCase("prod"))
		
		url="https://access.bpclcloud9.com/";
		System.out.println("URL is: "+url);

		try {
			driver.manage().window().maximize();
			driver.get(url);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			System.out.println("The browser launched successfully and loaded the url");
			System.out.println("Logging in as: "+user);
			reportStep("The browser launched successfully and loaded the url - "+url, "PASS");
			takeSnap(testName);
		} catch (WebDriverException e) {
			reportStep("FAIL", "Unable to launch the browser due to unknown error");
			takeSnap(testName);		
		}		
		System.out.println("Starting test: "+testName);
		
		enterByXpath("//input[@id='auth-login-address']", user, "UserName");
		enterByXpath("//input[@id='auth-login-password']", pwd, "Password");
		clickByXpath("//span[text()='Login']", "Login");
		clickByXpath("//span[text()='  Filter']", "Filter");
		clickByXpath("(//p[text()='All'])[3]", "ROID");
		enterByXpath("//input[@id='ro-panel-search']", "11932", "EnterROID");
		clickByXpath("//span[text()='AGRAWAL AUTOMOBILES - 11932']", "Agarwal Automobiles");
		clickByXpath("//span[text()='Apply']", "Apply");
		reportStep("Pass", "Launching Cadenz in '"+env+"' environment");
    	Thread.sleep(30000);}
    	

	public void enterByXpath(String xpathValue, String data, String fieldName) {
		testName = this.getClass().getSimpleName();
		try {
			driver.findElement(By.xpath(xpathValue)).sendKeys(data);
			System.out.println("The value '"+data+"' is entered in the field '"+fieldName+"'");
			reportStep("Pass","The value '"+data+"' is entered in the field '"+fieldName+"'");
			takeSnap(testName);
		} catch (NoSuchElementException e) {
			reportStep("Fail","The field '"+fieldName+"' is not found in DOM");
			System.err.println("The field '"+fieldName+"' is not found in DOM");
			takeSnap(testName);
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The field '"+fieldName+"' is not visible in the WebPage");
			takeSnap(testName);
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not interactable in the WebPage");
			takeSnap(testName);
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not selectable in the WebPage");
			takeSnap(testName);
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The field '"+fieldName+"' is no longer available in the WebPage");
			takeSnap(testName);
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with The field '"+fieldName+"'");
			takeSnap(testName);
		}
		}


	public void clickByXpath(String xpathVal, String fieldName) {
		testName = this.getClass().getSimpleName();
		try {
			driver.findElement(By.xpath(xpathVal)).click();
			System.out.println("The field '"+fieldName+"' is clicked");
			reportStep("Pass","The field '"+fieldName+"' is clicked");
			takeSnap(testName);
		} catch (NoSuchElementException e) {
			reportStep("Fail","The field '"+fieldName+"' is not found in DOM");
			takeSnap(testName);
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The field '"+fieldName+"' is not visible in the WebPage");
			takeSnap(testName);
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not interactable in the WebPage");
			takeSnap(testName);
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not selectable in the WebPage");
			takeSnap(testName);
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The field '"+fieldName+"' is no longer available in the WebPage");
			takeSnap(testName);
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with The field '"+fieldName+"'");
			takeSnap(testName);
		}
			
	}

	public void clickByXpathNoSnap(String xpathVal, String fieldName) {
	
		try {
			driver.findElement(By.xpath(xpathVal)).click();
			reportStep("Pass","The field with XPath '"+xpathVal+"' is clicked");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The field '"+fieldName+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The field '"+fieldName+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The field '"+fieldName+"' is no longer available in the WebPage");
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with The field '"+fieldName+"'");
		}
	}
	
	public String getTextByXpath(String xpathVal, String fieldName) {
		String value=null;
		testName = this.getClass().getSimpleName();
		try {
			 value=driver.findElement(By.xpath(xpathVal)).getText();
				reportStep("Pass","The text value at the element '"+fieldName+"' is: '"+value+"' ");
				takeSnap(testName);
		} catch (NoSuchElementException e) {
			reportStep("Fail","The field '"+fieldName+"' is not found in DOM");
			takeSnap(testName);
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The field '"+fieldName+"' is not visible in the WebPage");
			takeSnap(testName);
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not interactable in the WebPage");
			takeSnap(testName);
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not selectable in the WebPage");
			takeSnap(testName);
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The field '"+fieldName+"' is no longer available in the WebPage");
			takeSnap(testName);
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with The field '"+fieldName+"'");
			takeSnap(testName);
		}	
		
		return value;
	}
	
	public boolean isDisplayedByXpath(String xpathVal, String fieldName) {
		boolean value=false;
		testName = this.getClass().getSimpleName();
		try {
			 value=driver.findElement(By.xpath(xpathVal)).isDisplayed();
				reportStep("Pass","The element '"+fieldName+"' is: ' displayed");
				takeSnap(testName);
		} catch (NoSuchElementException e) {
			reportStep("Fail","The field '"+fieldName+"' is not found in DOM");
			takeSnap(testName);
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The field '"+fieldName+"' is not visible in the WebPage");
			takeSnap(testName);
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not interactable in the WebPage");
			takeSnap(testName);
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not selectable in the WebPage");
			takeSnap(testName);
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The field '"+fieldName+"' is no longer available in the WebPage");
			takeSnap(testName);
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with The field '"+fieldName+"'");
			takeSnap(testName);
		}	
		
		return value;
	}
	
	public void clickByCSS(String CSSVal, String fieldName) {
		testName = this.getClass().getSimpleName();
		try {
			driver.findElement(By.cssSelector(CSSVal)).click();
			reportStep("Pass","The field '"+fieldName+"' is clicked");
			takeSnap(testName);
		} catch (NoSuchElementException e) {
			reportStep("Fail","The field '"+fieldName+"' is not found in DOM");
			takeSnap(testName);
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The field '"+fieldName+"' is not visible in the WebPage");
			takeSnap(testName);
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not interactable in the WebPage");
			takeSnap(testName);
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The field '"+fieldName+"' is not selectable in the WebPage");
			takeSnap(testName);
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The field '"+fieldName+"' is no longer available in the WebPage");
			takeSnap(testName);
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with The field '"+fieldName+"'");
		}
	}
	
	public void clickById(String id) {
		
		try {
			driver.findElement(By.id(id)).click();
			takeSnap(testName);
			reportStep("Pass","The field with id '"+id+"' is clicked");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with Id '"+id+"' is not found in DOM");
			takeSnap(testName);
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Id '"+id+"' is not visible in the WebPage");
			takeSnap(testName);
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Id '"+id+"' is not interactable in the WebPage");
			takeSnap(testName);
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Id '"+id+"' is not selectable in the WebPage");
			takeSnap(testName);
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Id '"+id+"' is no longer available in the WebPage");
			takeSnap(testName);
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Id '"+id+"'");
			takeSnap(testName);
		}
		
	}
	
	

	public void closeAllBrowsers() {
		 try { 
				driver.close();	
				System.out.println("Closing browser");
				reportStep("Pass","Browser is closed successfully");
				driver.quit();
				 }
				 catch(NoSuchWindowException e) {
					 reportStep("Fail","No window/windows found");
				 }
				 catch(WebDriverException e) {
					 reportStep("Fail","Error while closing the browser window/windows");
				 }
	}


	
	
	/*public void verifyTextById(String id, String text) {
		try {
			String textValue=driver.findElementById(id).getText();
			if(textValue.equals(text)) {
				reportStep("Pass","The text value "+text+"' is as expected");
			}
				else {
					reportStep("Pass","The expected text value at the field '"+id+"' is '"+text+"' but the actual text is '"+textValue+"'");	
			}
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with Id '"+id+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Id '"+id+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Id '"+id+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Id '"+id+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Id '"+id+"' is no longer available in the WebPage");
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Id '"+id);
		}
		}

	public void verifyTextByXpath(String xpath, String text) {
		try {
			String textValue=driver.findElement(xpath).getText();
			if(textValue.equals(text)) {
				reportStep("Pass","The text value "+text+"' is as expected");
			}
				else {
					reportStep("Pass","The expected text value at the field '"+xpath+"' is '"+text+"' but the actual text is '"+textValue+"'");	
			}
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is no longer available in the WebPage");
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with XPath '"+xpath);
		}
}

	public void verifyTextContainsByXpath(String xpath, String text) {
	
		try {
			String textValue=driver.findElement(xpath).getText();
			if(textValue.contains(text)) {
				reportStep("Pass","The text value "+text+"' is as expected");
			}
				else {
					reportStep("Pass","The actual text '"+textValue+"' does not contain the expected text '"+text+"'");	
			}
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with XPath '"+xpath+"' is no longer available in the WebPage");
		}  catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with XPath '"+xpath);
		}
	}

	

	public void clickByClassName(String classVal) {
	
		try {
			driver.findElementByClassName(classVal).click();
			reportStep("Pass","The field with ClassName '"+classVal+"' is clicked");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with ClassName '"+classVal+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with ClassName '"+classVal+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with ClassName '"+classVal+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with ClassName '"+classVal+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with ClassName '"+classVal+"' is no longer available in the WebPage");
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with ClassName '"+classVal);
		}
			
	}

	public void clickByName(String name) {
		try {
			driver.findElementByName(name).click();
			reportStep("Pass","The field with Name '"+name+"' is clicked");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with Name '"+name+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Name '"+name+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Name '"+name+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Name '"+name+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Name '"+name+"' is no longer available in the WebPage");
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Name '"+name);
		}
		
	}

	public void clickByLink(String name) {
		try {
			driver.findElementByLinkText(name).click();
			reportStep("Pass","The field with Link '"+name+"' is clicked");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with Link '"+name+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Link '"+name+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Link '"+name+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Link '"+name+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Link '"+name+"' is no longer available in the WebPage");
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Link '"+name);
		}
			
	}

	public void clickByLinkNoSnap(String name) {
		try {
			driver.findElementByLinkText(name).click();
			reportStep("Pass","The field with Link '"+name+"' is clicked");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with Link '"+name+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Link '"+name+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Link '"+name+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Link '"+name+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Link '"+name+"' is no longer available in the WebPage");
		} 	
		catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Link '"+name);
		}
		
		
	}


	public String getTextById(String idVal) {
		String textValue=null;
		try {
			 textValue=driver.findElementById(idVal).getText();
				reportStep("Pass","The text value at the id '"+idVal+"'is "+textValue+"' ");
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with Id '"+idVal+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Id '"+idVal+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Id '"+idVal+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Id '"+idVal+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Id '"+idVal+"' is no longer available in the WebPage");
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Id '"+idVal);
		}
				
		return textValue;
		
	}*/

		/*public void selectVisibileTextById(String id, String value) {
		try {
		WebElement element=driver.findElementById(id);
		Select elementObj=new Select(element);
		elementObj.selectByVisibleText(value);
		reportStep("Pass","The dropdown with id '"+id+"' is found and the value '"+value+"' is selected from the dropdown.");
		}
		catch (NoSuchElementException e) {
			reportStep("Fail","The element with Id '"+id+"' is not found in DOM");
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Id '"+id+"' is not visible in the WebPage");
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Id '"+id+"' is not interactable in the WebPage");
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Id '"+id+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Id '"+id+"' is no longer available in the WebPage");
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Id '"+id);
		}
	}

	public void selectIndexById(String id, int value) {
	
		try {
			WebElement element=driver.findElementById(id);
			Select elementObj=new Select(element);
			elementObj.selectByIndex(value);
			reportStep("Pass","The dropdown with id '"+id+"' is found and the value at the position'"+value+"' is selected from the dropdown.");
			}
			catch (NoSuchElementException e) {
				reportStep("Fail","The element with Id '"+id+"' is not found in DOM");
			} catch (ElementNotVisibleException e) {
				reportStep("Fail","The element with Id '"+id+"' is not visible in the WebPage");
			} catch (ElementNotInteractableException e) {
				reportStep("Fail","The element with Id '"+id+"' is not interactable in the WebPage");
			} catch (ElementNotSelectableException e) {
				reportStep("Fail","The element with Id '"+id+"' is not selectable in the WebPage");
			} catch (StaleElementReferenceException e) {
				reportStep("Fail","The element with Id '"+id+"' is no longer available in the WebPage");
			} catch (WebDriverException e) {
				reportStep("Fail","Not able to find/interact with the element with Id '"+id);
			}
				
	}

	public void switchToParentWindow() {
		try {
		Set<String> winIDs=driver.getWindowHandles();
		for(String winID:winIDs) {
			driver.switchTo().window(winID);
			break;
		}
		reportStep("Pass","Switched to the last available Window");
		}
		catch (NoSuchWindowException e) {
			reportStep("Fail","The Parent window was not availble");
		}
		catch(NoSuchSessionException e) {
			reportStep("Fail","The session for Parent window is not available");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Problem encountered while switching to the Parent window");
		}
		
	}

	public void switchToLastWindow() {
		
		try {
		Set<String> winIDs=driver.getWindowHandles();
		for(String winID:winIDs) {
			driver.switchTo().window(winID);
		}
		reportStep("Pass","Switched to the last available Window");
		}
		catch (NoSuchWindowException e) {
			reportStep("Fail","The window was not availble");
		}
		catch(NoSuchSessionException e) {
			reportStep("Fail","The session is not available");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Problem encountered while switching to the last window");
		}
			 
		
	}

	public void acceptAlert() {
	
		try {
			driver.switchTo().alert().accept();
			reportStep("Pass","The alert is accepted successfully");
		}catch(NoAlertPresentException e) {
			reportStep("Fail","Alert is not present in the window");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Error occured while accepting the Alert");
		}
		
	}

	public void dismissAlert() {
	  
		try {
			driver.switchTo().alert().dismiss();
			reportStep("Pass","The alert is dismissed successfully");
			
		}catch(NoAlertPresentException e) {
			reportStep("Fail","Alert is not present in the window");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Error occured while dismisisng the Alert");
		}
		
	}

	public String getAlertText() {
		String alertMessage=null;
		
		try {
			alertMessage= driver.switchTo().alert().getText();
			reportStep("Pass","The alert message is '"+alertMessage+"'");
		}catch(NoAlertPresentException e) {
			reportStep("Fail","Alert is not present in the window");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Error occured while switching to the Alert");
		}
		return alertMessage;
	}*/

	public void takeSnap(String testName) {
		// i=0;
	 try {
		File tmp= ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String filepath="./screenshots/"+testName+"/screen"+i+".jpeg";
		 File dest= new File(filepath);
		 FileUtils.copyFile(tmp, dest);
	} catch (WebDriverException e) {
	reportStep("Fail","Error while taking screenshot");
	
	} catch (IOException e) {
	reportStep("Fail","Error while creating/copying the image file");
	}
		i++;
	}

	public void closeBrowser() {
	 try { 
	driver.close();	
	reportStep("Pass","The browser is closed successfully");
	 }
	 catch(NoSuchWindowException e) {
		 reportStep("Fail","No window found");
	 }
	 catch(WebDriverException e) {
		 reportStep("Fail","Error while closing the browser");
	 }
	}
	public void enterByIdTab(String idValue, String data,String fieldName) {
		try {
			driver.findElement(By.id(idValue)).sendKeys(data);
			reportStep("Pass","The value '"+data+"' is entered in the field '"+fieldName+" with id '"+idValue+"'");
			takeSnap(testName);
		} catch (NoSuchElementException e) {
			reportStep("Fail","The element with Id '"+idValue+"' is not found in DOM");
			takeSnap(testName);
		} catch (ElementNotVisibleException e) {
			reportStep("Fail","The element with Id '"+idValue+"' is not visible in the WebPage");
			takeSnap(testName);
		} catch (ElementNotInteractableException e) {
			reportStep("Fail","The element with Id '"+idValue+"' is not interactable in the WebPage");
			takeSnap(testName);
		} catch (ElementNotSelectableException e) {
			reportStep("Fail","The element with Id '"+idValue+"' is not selectable in the WebPage");
		} catch (StaleElementReferenceException e) {
			reportStep("Fail","The element with Id '"+idValue+"' is no longer available in the WebPage");
		} catch (WebDriverException e) {
			reportStep("Fail","Not able to find/interact with the element with Id '"+idValue);
		}
		
		
	}

/*	public void selectIndexByXpath(String xpathValue, int index) {
	try {
		WebElement element=driver.findElement(xpathValue);
		Select obj=new Select(element);
		obj.selectByIndex(index);
		reportStep("Pass","IRCTC Lounge Element' is clicked and the dropdown value at index '"+index+"' is selected");
	}
		catch(NoSuchElementException e) {
			reportStep("Fail","IRCTC Executive lounge Element' not found");
		}
		catch(ElementNotVisibleException e)
		{
			reportStep("Fail","Element' not Visible");
		}
		catch(ElementNotInteractableException e) {
			reportStep("Fail","Element' not interactable");
		}
		catch(ElementNotSelectableException e)
		{
			reportStep("Fail","Element' not Selectable");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Error while clicking on element'");
		}
	

	}

	public void selectVisibleTextByXpath(String xpathValue, String text) {
		try {
		WebElement element=driver.findElement(xpathValue);
		Select obj=new Select(element);
		obj.selectByVisibleText(text);
		reportStep("Pass","Element' is clicked and the dropdown value with value '"+text+"' is selected");
		}
		catch(NoSuchElementException e) {
			reportStep("Fail","Element' not found");
		}
		catch(ElementNotVisibleException e)
		{
			reportStep("Fail","Element' not Visible");
		}
		catch(ElementNotInteractableException e) {
			reportStep("Fail","Element' not interactable");
		}
		catch(ElementNotSelectableException e)
		{
			reportStep("Fail","Element' not Selectable");
		}
		catch(WebDriverException e) {
			reportStep("Fail","Error while clicking on element'");
		}
		 
			 
		
		
	}*/
}
