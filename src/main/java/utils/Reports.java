package utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Properties;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Reports {
	
	public static ExtentReports report;
	public ExtentTest test;
	
	String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new java.util.Date());
			
	/*
	 * DateTimeFormatter dtf = DateTimeFormatter.ofPattern("ddMMMYYYY'T'HHmmss");
	 * LocalDate now = LocalDate.now();
	 */   
	//String reportName="./testreports/TestReport_"+timeStamp+".html";
	public void startReport() throws IOException {
		Properties property1=new Properties();
	  	BufferedReader reader=new BufferedReader(new FileReader("./Data/buildDetails.properties"));
		property1.load(reader);  
		//String num=property1.getProperty("build");
		//String name=property1.getProperty("name");
		String name = System.getenv("BUILD_NUMBER");
		String newTestName="TestReport_"+name+".html";
		String reportName="./testreports/"+newTestName;
		System.out.println("Report name is: "+newTestName);
		
		report=new ExtentReports(reportName,false);	
	//	Locale.setDefault(Locale.ENGLISH);
	}
	
	public void startTest(String testName,  String description) {
		//Locale.setDefault(Locale.ENGLISH);
		test=report.startTest(testName, description);
	}
	
	public void reportStep(String status, String desc) {
		
		if(status.equalsIgnoreCase("pass")) {
		test.log(LogStatus.PASS, desc);
		}
		else if(status.equalsIgnoreCase("fail")) {
			test.log(LogStatus.FAIL, desc);
		}
	}
	
	public void endTest() {
		report.endTest(test);
	}
	
	public void endReport() {
		report.flush();
	}

}
