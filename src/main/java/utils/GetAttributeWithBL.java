package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.apache.commons.lang3.text.WordUtils;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;

public class GetAttributeWithBL {


	public static ArrayList<String> getAttributesWithBlacklist(String sheetName, String role) throws IOException {
		String attributeList = null;
		try {
			FileInputStream fis = new FileInputStream("./Data/AttributesRoles.xlsx");

			XSSFWorkbook wb = new XSSFWorkbook(fis);
			XSSFSheet sheet=	wb.getSheet(sheetName);
			int rowCount=sheet.getLastRowNum();
			int rowNumber=0;
			for(int i=0;i<rowCount+1;i++)
			{
				XSSFRow row=sheet.getRow(i);
				String roleAtI=sheet.getRow(i).getCell(0).getStringCellValue();
				if(roleAtI.equals(role)) {
					rowNumber=i;
					break;
				}
			}

			attributeList=sheet.getRow(rowNumber).getCell(1).getStringCellValue();
			wb.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		String finalAttributeList=attributeList.replaceAll("_", " ");
		System.out.println("***Returning TestData***");
		String[] attributeArray=finalAttributeList.split(",");
		int len=attributeArray.length;
		String[] capArray=new String[len];
		for (int i=0;i<len;i++)
		{
			capArray[i]=	WordUtils.capitalize(attributeArray[i]);
		}
		ArrayList<String> excelAttList=new ArrayList<String>(Arrays.asList(capArray));
		Collections.sort(excelAttList);

		return excelAttList;
	}




}
