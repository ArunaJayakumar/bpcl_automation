package smokeTests;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xdgf.usermodel.section.GeometrySection;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.google.common.base.Function;

import utils.ProjectWrappers;

public class TC002_CreateTank extends ProjectWrappers{
	
	@BeforeClass
	public void beforeClass() {
	
		testName = this.getClass().getSimpleName();
		description="To verify Home page";
		author="Aruna";
		category="Smoke";
			}
	
	public WebDriver driver;
	//public int i;
	@Test (alwaysRun = true)
	public void AudienceTestFilter() throws InterruptedException, IOException {
		driver=getDriver();
		
    	invokeApp(testName, true);
	
    	Thread.sleep(7000);
    	if(isDisplayedByXpath("//span[text()='Close']", "Review Comment")) {
    		clickByXpath("//span[text()='Close']", "Close Review Comment");
    	}
    	clickByXpath("(//span[text()='Tanks'])[1]", "Tanks");
    	clickByXpath("//span[text()='  Add']", "Add");
    	clickByCSS("h2[class='MuiTypography-root MuiTypography-h6']>svg", "Maximize Create Tank");
    	clickByXpath("//div[@id='mui-component-select-product']", "Product dropdown");
    	clickByXpath("//li[text()='SPEED']", "SPEED");
    	enterByXpath("(//input[@id='outlined-multiline-static'])[4]", "2300", "Diameter");
    	clickByXpath("//div[@id='mui-component-select-capacity']", "Capacity Dropdown");
    	clickByXpath("//li[text()='15000']", "15000");

    	clickByXpath("//span[text()='Add']", "Add");
    	Thread.sleep(1000);

    	if(isDisplayedByXpath("//span[text()='Tank Created/Updated Successfully!']", "Create Tank Success Message")) {
    		reportStep("pass", "Tank created successfully");
    	}

    	
	}
}
