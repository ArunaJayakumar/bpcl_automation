package smokeTests;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xdgf.usermodel.section.GeometrySection;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.google.common.base.Function;

import utils.ProjectWrappers;

public class TC005_SendForApproval extends ProjectWrappers{
	
	@BeforeClass
	public void beforeClass() {
	
		testName = "TC005_SendForApproval";
		description="To send the equipments for approval";
		author="Aruna";
		category="Smoke";
			}
	
	public WebDriver driver;
	//public int i;
	@Test (alwaysRun = true)
	public void AudienceTestFilter() throws InterruptedException, IOException {
		driver=getDriver();
		
    	invokeApp(testName, true);
	
    	Thread.sleep(3000);
    	
    	clickByXpath("//span[text()='  Send For Approval']", "Send For Approval");
    	
	}
}
